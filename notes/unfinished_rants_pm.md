Read below at your own risk.
As of now it is neither:  
* balanced,
* finished,
* well-written,
* worth sharing.


# Posssible beginning

If you are smart, creative with mind full of your own ideas - don't even consider academia as a career.
Perhaps you were lured there by:
* Freedom,
* Meritocracy,
* Changing the world. 
In particular, if you are an individualist, stay the fuck away from it.

...



OR

Coming from a department which is didactically great (at least in my opinion), but at the same time wrecks havoc among careers. There is nothing like ways to see internships in other places, or career choices outside of the univ. (regardless if academic or not).
Once I was even said shamelessly that there is no point of telling students about PhD overseas - if they aren't smart - there is not point of wasting time on them; if they are - there is no point of letting them go. 
I should have said "I guess, from professors perspective there is not point of not-harvesting kidney from a student, I guess?" but I was to slow.s


For me it was one of the main motivations to run SKFiz UW - to spread info, etc.

In particular, I h


(seriously depressed)

OR

Academia seems to be broken - as many things are far from optimum, due to historical, political or structural reasons. 

(The problem seems to be much more severe for education, and important, as it influences life of virtually everyone, but it is another story.)

# My personal


There have been years of low productivity and frustration.
* Initially, because of not knowing what is the game about. 
* Later, because having no sense of being on track. I desperately tried to move into complex systems, but lacked of networking there.

Different people may have different motivations, but for my 

OR

"Academia is as good for innovative research as schools is for learning"


# Rant-like

Academia, screw you!

For me, it has been 5 years of trying to jump into complex systems, without a success. I had backgrounds in physics, so it should be simple... I was mailing authors of papers I liked, I was talking to conference speakers whose topics were interesting; and it didn't result in any collaboration. Sure, most of the conversations were warm and interesting, but nowhere near to actually going into a collaboration.

Maybe I was not smart enough?
Well, it was not pure mathematics or string theory, so I would be surprised not to be in the top 20%.

Maybe I was not creative or independent enough?
Well, skim my website and judge for yourself.

Maybe I was not productive or successful enough?
Well, sure there are guys with more publications; however, when it was an undergraduate student with a few publications (some of them as the first author), it was hardly any different.

I was disappointed, as I had 0% success rate plus no idea what I was doing wrong. I also tried to apply for some.

Well, it took a few years to realize a few things, 
* friend of a friend > meritocracy,
* academic inertia,
* slave system,
* creativity of independence not expected from young researchers.

Sure, merit in academia is important, and were you are not intelligent enough, contacts won't help. But the sad thing is that when you are not networked enough, no one will even go to the level when they see your skills. There is a totally different thing whether someone is engage into a discussion (and most of academicians are open with that respect) and do anything which requires paperwork or funding (to take someone on a collaboration).

(I am curious whether is it like that people are to lazy to grade applicants and just say "if someone is a student of X", (s)he needs to be good, or are basically cowards and will never say "no" to a big guy in their field, even if other more talented individuals have applied as well.)

Quite a few times I have heard very straightforwardly. 

(And politics is something which depends on two key factors: social skills (which I have high... as for someone with Asperger's Syndrome...) and laying (which I contempt, especially in science, which supposed to be the exact opposite))

People are event surprised that I have also other research projects than my PhD-related. And that I have ideas on my own... (and I am surprised as well... I hoped to find at least some independent minds among PhD students, at they turned to be a very rare phenomenon, on the verge of extinction).

Moreover, it seems that independence and creativity are _bad_ traits for a PhD student. If there is an intelligent and productive guy, (s)he makes a great material for a slave worker, boosting boss's projects, grant proposals, and boss's fame; and there is not risk that (s)he will fall into h(er/is) projects.  



but I disregarded the main aspect: it is not about meritocracy - the most important thing is whom 


In short, I have learnt the hard way how academia works in practice... or at least how it worked for me.

And I wholeheartedly hope that the current academic system will collapse. 

---

If you have your own ideas, own taste, and are not a generic mindless sheep how will seamlessly treat anything which is on hype as being really important... well, then you have a problem. You are in so narrow minority, that no-one cares. And not many people will want to collaborate, leave alone funding (unless, in both cases, you put yours field buzzwords & empty phrases like raisins into a cake).

So if your motivation for doing science is "well, I have my own problems you would love tu pursue" (rather "I have no ideas, but will do anything that other consider important"), then academia is just a bad place to do it (much better have a bounded-time job, and some time and money to pursue your true interests on your own).