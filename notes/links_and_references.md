# About

For links which are not inlined. Especially for general references.

Ideally:

* [Intergalactic unemployed - The e-Con-o'mist](http://example.com/2012-99/aaa)
    * An analysis of unemployment rate of PhDs in intergalactic trading...
    * PM: A good thing for the introduction. 


# Links

* http://www.economist.com/node/17723223
* http://liv.dreamwidth.org/389934.html and related discussion http://news.ycombinator.com/item?id=5098873
    * This one is particularly nice: http://liv.dreamwidth.org/389934.html?thread=4571694#cmt4571694
* http://pgbovine.net/PhD-memoir.htm
* that text with passionate worker
* http://membracid.wordpress.com/2007/07/28/academia-is-a-cult/
* http://www.guardian.co.uk/science/occams-corner/2013/apr/02/1
* academic freedom - http://matt-welsh.blogspot.com.es/2013/04/the-other-side-of-academic-freedom.html
* Schroedinger's Rat
* http://marcua.net/writing/gradschool-guide/
    * Text template / HTML style seems to be pleasant
* http://contemplativemammoth.wordpress.com/2013/06/19/academia-doesnt-have-a-phd-problem-it-has-an-attitude-problem/
    * Defending the current status quo with PhD studies and lack of academic positions
* http://academia.stackexchange.com/questions/10969/strategies-to-overcome-academic-apathy-in-the-final-stages-of-the-phd
    * And other popular topics from academia.SE about frustration and burnout
* http://wroclaw.gazeta.pl/wroclaw/1,35751,14224930,Studenci__ktorzy_beda_milionerami__Powstaja_elitarne.html
    * (raczej o edukacji i po polsku) 
* http://alexwarren.co.uk/2013/07/08/life-is-not-a-conveyor-belt/
    * on obedience (more about education than science... but still)
* http://www.slate.com/articles/double_x/doublex/2013/06/female_academics_pay_a_heavy_baby_penalty.html
	* and other about overcompetiveness and problems with having a family
* http://universityoflies.wordpress.com/2013/02/26/3-questions-you-should-never-ask-a-phd-student/ 
	* questions, humorous 
* http://blogs.scientificamerican.com/guest-blog/2013/07/21/the-awesomest-7-year-postdoc-or-how-i-learned-to-stop-worrying-and-love-the-tenure-track-faculty-life/
    * work-life balance in academia 
* http://www.smbc-comics.com/?id=3059#comic
    * on tenure and freedom 
* http://andrewdumont.me/avoiding-burnout 
    * actually for programmers, but many points are the same
    * https://news.ycombinator.com/item?id=5630445
* http://cs.unm.edu/~terran/academic_blog/?p=113
    * on leaving academia (and academia vs Google); very good!
    * https://news.ycombinator.com/item?id=4287604 (some points on grants, bureaucracy and effective lack of freedom)  

...hackathons, open source, etc
...learning ull facts is considered hard work, reading wikipedia- procrastination