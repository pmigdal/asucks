# Metaphors and decorations

* A moth going into light
* A lich, powerful but devoid of life
* Like brewing coffee - extracting bitterness
* The grand prize in science (if a risky projects takes) is not changing the World, or billions -  it's merely winning a toaster.