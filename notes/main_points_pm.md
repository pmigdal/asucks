Why academia and education suck?

* Hierarchical structure
    * Blocking innovations
    * Blocking free market
    * Making things 1-2 generations back
    * Intertia (with admissions, grants, etc)
    * Smarter are ruled by dumper
* Recommendations, FoaF
* Measurable correlates, diplomas, etc
* Myths based on _previous_ systems, .currently less freedom and places
* Low security and survival chances (and slavery)
* "School" vs "learning for oneself"
* Long-term commitment, all-or-nothing
* Overcompetitive (which kills creativity)
    * Role of noise, insight, broad perspective
    * Haste, the same thing
* Monoculture - lecturers are almost only from academia, not target companies
* Somehow concave strategy in a convex thing! 
* Advertisement instead of sincere intuitions and caveats

# School

* Leveling down
* Everyone expect a degree
* For title, diploma, grades, not _learning_
* Rigid
* Not promoting independence, out of the box interests or approaches


# And features, not bugs

* The longer you spend on a problem, it may be worth more (so no easy cut-off)
* No easy reality-checks
* Progress is orders of magnitude slower, for a single human perspective (which is relevant to motivation, opportunities, feedback, etc), than in programming
* If you want to change the world, you can start being 12yo when programmer; when an academician - perhaps 35yo (to learn enough, to get position, to get funds so you wont starve)

# Vs programming

* Recommendations vs Interviews
* "Geeky offers", hackathons, etc 
* See also: this entry about a guy working alone as a programmer
* Programmers keep creating world in which they live
    * Scientists also (see: organizing conferences, seminars) but to a lesser degree

# Raisins

"But lack of hard requirements will produce a lot of crappy scientists!"
"Sure! But also a lot of independent ones, having truly genuine ideas, not just being a serial product of many outdated requirements. Again, it is a _very_ convex job."

# What it is vs what it claims to be:

* research & education -> grant writing
* search for the truth -> self/field-advertising
* science -> high-end engineering
* intellectual freedom -> ..as long as it is a fashionable topic (sure, you are able to do anything else, just you will need luck to get nay )

# Psychostuff

* healthier to "sell out" than deluding oneself (or constantly failing at achieving one's dream)
* Working on someone's idea but being expected to have motivation to work as if it was your own (and hey, we don't need to pay except for food and accommodation, because it is your hobby, isn't it?)