# Asucks

Notes turning slowely into an essay on the condition of academia.
Especially pointing the weak points of 'the system' and we can change it.
Let's keep it well-researched, well-balanced and well-written
(not 'yet another rant', or 'yet another one-data point example',
or 'yet another collection of random remarks').

* Preferred format: 
    * Markdown (.md) for text notes

* Structure:
    * drafts for actual drafts
    * notes for supplementary material, observations, links, etc
    * . for meta things (e.g. other parts of the project)
