OK, what are wee doing:


# Texts, right now


## Structural problems of the academia

Many problems and issues are common for all disciplines and fields.

* PM: I think it is the best one to start with.


## Good and bad parts of a (theoretical) PhD

Nice topic, but there are already many rants + it is better to try to analyze a problem than just put a single experience written as if it were something more general.

Also, there are considerable differences among fields (even theoretical physics vs experimental physics, not even comparing with humanities) and they may be dependent on place:  
* funding,
* job opportunities after graduating,
* time it takes to finish,
* teaching type and burden,
* one's life situation,
* prestige of the institute and the advisor,
* one's success (for any reason).


## My PhD/academia experience

IMHO better to have a single good case-study than a shallow (and biased) statistics.
However: 
* There are many such.
* One needs to balance very well to be for the use by others.


# Other ideas and projects

## Webpage "The PhD Experience"
(And can be creeped into a general job testimonial)

(A website where anyone can post PhD testimonial, form different branch, etc;
positive or negative
"erowid.org for trips into knowledge" )

* Life motivation (before starting)
* Background
* Biggest issues
* Biggest joys
* Story
* Would I recommend it?